// Copyright (С) ABBYY (BIT Software), 1993 - 2012. All rights reserved. 

#import "RecognitionViewController.h"
#import <MocrEngine.h>

static NSString* commonHtmlTitle = @"<font size=\"10\">";

static NSString* FineBcrFieldNames[MBFT_Count] = {
	@"Phone: ",
	@"Fax: ",
	@"Mobile: ",
	@"E-mail: ",
	@"Web: ",
	@"Address: ",
	@"Name: ",
	@"Company: ",
	@"Job: ",
	@"Other Text: ",
};

static NSString* FineBcrFieldComponentNames[MBFCT_Count] = {
	@"First Name:",
	@"Middle Name:",
	@"Last Name:",
	@"Extra Name:",
	@"Title:",
	@"Degree:",
	@"Phone Prefix:",
	@"Phone Country Code:",
	@"Phone Code:",
	@"Phone Body:",
	@"Phone Extensoin:",
	@"Zip Code:",
	@"Country:",
	@"City:",
	@"Street Address:",
	@"Region:",
	@"Job Position:",
	@"Job Department:",
};

@implementation CRecognitionViewController

@synthesize imageView;
@synthesize resultsView;

- (id) init
{
	self = [super initWithNibName:@"RecognitionView" bundle:nil];
	if( self == nil ) {
		return nil;
	}

	_pathToData = nil;

	[self initializeRecognitionEngine];
	
	_recognitionService = [[CRecognitionService alloc] init];

	return self;
}

-(void)viewDidLoad
{
	[super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
//	[self recognizeBusinessCard:imageView.image];
}

- (void) initializeRecognitionEngine
{
	// Load the license data from file.
	NSString* licenseDataFilePath = [[self pathToData] stringByAppendingPathComponent:@"license"];
	NSFileHandle* licenseDataFile = [NSFileHandle fileHandleForReadingAtPath:licenseDataFilePath];
	NSData* licenseData = [licenseDataFile readDataToEndOfFile];
	
	// Intialize MobileOCR engine
	NSArray* dataSources = [NSArray arrayWithObject:[CMocrDirectoryDataSource dataSourceWithDirectoryPath:[self pathToData]]];
	CMocrLicense* license = [CMocrLicense licenseWithLicenseData:licenseData applicationId:@"iOS_ID"];

	_mocrEngine = [CMocrEngine createArcSharedEngineWithDataSources:dataSources license:license];
	if( _mocrEngine == nil ) {
		// Failed to create singleton instance of the CMocrEngine class.
		TMocrErrorCode errorCode;
		NSString* errorMessage;
		[CMocrEngine getLastError:&errorCode message:&errorMessage];
		NSLog(@"Error code: %@. Error message: %@", [CRecognitionViewController stringFromMocrErrorCode:errorCode], errorMessage);
	}
	
	_ocrConfiguration = nil;
	_bcrConfiguration = nil;
}

- (void) dealloc 
{
	if( ![CMocrEngine destroySharedInstance] ) {
		// CMocrEngine destruction failed.
		// You can check CMocrEngine last error.
	}
	
}

- (NSString*) pathToData
{
	if( _pathToData == nil ) {
		NSBundle* mainBundle = [NSBundle mainBundle];
		if( mainBundle != nil ) {
			NSString* bundlePath = [mainBundle bundlePath];
			_pathToData = [bundlePath copy];
		} else {
			_pathToData = @"./";
		}
	}
	return _pathToData;
}

- (CMocrRecognitionConfiguration*) ocrConfiguration
{
	if( _ocrConfiguration == nil ) {
		NSSet* recognitionLanguages = [NSSet setWithObjects:@"English", nil];
		_ocrConfiguration = [[CMocrRecognitionConfiguration alloc]
							initWithImageResolution:0
							imageProcessingOptions:MIPO_DetectPageOrientation | MIPO_ProhibitVerticalCjkText
							recognitionMode:MRM_Full
							recognitionConfidenceLevel:MRCL_Level3
							barcodeTypes:MBT_ANY1D | MBT_SQUARE2D | MBT_PDF417
							defaultCodePage:MSCP_Utf8
							unknownLetter:L'^'
							recognitionLanguages:recognitionLanguages];
	}
	return _ocrConfiguration;
}

- (CMocrRecognitionConfiguration*) bcrConfiguration
{
	if( _bcrConfiguration == nil ) {
		NSSet* recognitionLanguages = [NSSet setWithObjects:@"English", nil];
		_bcrConfiguration = [[CMocrRecognitionConfiguration alloc]
							initWithImageResolution:0
							 imageProcessingOptions:0
									recognitionMode:MRM_Full
						 recognitionConfidenceLevel:MRCL_Level3
									   barcodeTypes:0
									defaultCodePage:MSCP_Utf8
									  unknownLetter:L'^'
							   recognitionLanguages:recognitionLanguages];
	}
	return _bcrConfiguration;
}

// Represent CMocrLayout as html string to show in UIWebView.
- (NSString*) htmlFromMocrLayout:(CMocrLayout*)layout
{
	NSMutableString* resultString = [NSMutableString string];
	
	NSArray* layoutStrings = [layout copyStrings];
	for( NSString* string in layoutStrings ) {
		// Escaping HTML special chars from ascii range
		// see http://www.w3.org/TR/xhtml1/dtds.html#a_dtd_Special_characters
		NSString* temp = string;
		temp = [temp stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"];	// &#34 -> &quot;
		temp = [temp stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];	// &#38 -> &amp;
		temp = [temp stringByReplacingOccurrencesOfString:@"'" withString:@"&apos;"];	// &#39 -> &apos;
		temp = [temp stringByReplacingOccurrencesOfString:@"<" withString:@"&lt;"];		// &#60 -> &lt;
		temp = [temp stringByReplacingOccurrencesOfString:@">" withString:@"&gt;"];		// &#62 -> &gt;
		[resultString appendFormat:@"%@<br/>", temp];
	}
	
	return resultString;
}

- (NSString*) businessCardFieldToHtmlString:(CMocrBusinessCardField*)businessCardField
{
	NSMutableString* resultString = [NSMutableString string];
	
	{
		NSString* fieldName = FineBcrFieldNames[businessCardField.fieldType];
		[resultString appendFormat:@"<font color=\"0088AA\">%@<font color=\"0000AA\">", fieldName];
		
		for(CMocrTextLine* textLine in businessCardField.lines) {
			NSString* lineString = [textLine copyString];
			[resultString appendFormat:@"%@<br/>", lineString];
		}
	}
	
	// How to work with field component.
	// If bcr-field is complex, then Components array contains more than zero of elements.
	// For each component, output it.
	for( CMocrBusinessCardFieldComponent* component in businessCardField.components ) {
		NSString* componentName = FineBcrFieldComponentNames[component.componentType];
		[resultString appendFormat:@"<font color=\"0088AA\"> * %@<font color=\"0000AA\">", componentName];
		
		for(CMocrTextLine* textLine in component.lines) {
			NSString* lineString = [textLine copyString];
			[resultString appendFormat:@"%@<br/>", lineString];
		}
	}
	
	return resultString;
}

// Represent CMocrBusinessCard as html string to show in UIWebView.
- (NSString*) htmlFromMocrBusinessCard:(CMocrBusinessCard*)businessCard
{
	NSMutableString* resultString = [NSMutableString string];
	
	for(CMocrBusinessCardField* bcrField in businessCard.fields) {
		[resultString appendString:[self businessCardFieldToHtmlString:bcrField]];
	}
	return resultString;
}

// Represent CMocrBarcode as html string to show in UIWebView.
- (NSMutableString*) htmlFromMocrBarcode:(CMocrBarcode*)barcode
{
	NSMutableString* resultString = [NSMutableString string];

	NSString* lineString = [barcode.text copyString];
	[resultString appendFormat:@"%@<br/>", lineString];

	return resultString;
}

+ (NSString*) stringFromMocrErrorCode:(TMocrErrorCode)errorCode
{
	switch( errorCode ) {
	case MEC_NoError:
		return @"MEC_NoError";
	case MEC_EngineInstanceExists:
		return @"MEC_EngineInstanceExists";
	case MEC_EngineNotInitialized:
		return @"MEC_EngineNotInitialized";
	case MEC_FileNotFound:
		return @"MEC_FileNotFound";
	case MEC_InvalidArgument:
		return @"MEC_InvalidArgument";
	case MEC_MemoryAllocationFailed:
		return @"MEC_MemoryAllocationFailed";
	case MEC_RecognitionInProgress:
		return @"MEC_RecognitionInProgress";
	case MEC_FineErrNotInitialized:
		return @"MEC_FineErrNotInitialized";
	case MEC_FineErrLicenseError:
		return @"MEC_FineErrLicenseError";
	case MEC_FineErrInvalidArgument:
		return @"MEC_FineErrInvalidArgument";
	case MEC_FineErrInternalFailure:
		return @"MEC_FineErrInternalFailure";
	case MEC_FineErrNotEnoughMemory:
		return @"MEC_FineErrNotEnoughMemory";
	case MEC_FineErrTerminatedByCallback:
		return @"MEC_FineErrTerminatedByCallback";
	}
}

- (void) showResults:(NSString*)stringToOutput
{
	NSMutableString* htmlString = [NSMutableString stringWithString:commonHtmlTitle];
	[htmlString appendString:stringToOutput];
	
	[resultsView loadHTMLString:htmlString baseURL:nil];
}

- (void) showMocrError:(TMocrErrorCode)errorCode message:(NSString*)errorMessage
{
	errorMessage = [[errorMessage stringByReplacingOccurrencesOfString:@"<" withString:@"&lt;"]
		stringByReplacingOccurrencesOfString:@">" withString:@"&gt;"];
	NSString* htmlMessage = [NSString stringWithFormat:
							 @"%@<h5>Error code</h5><p>%@</p><h5>Error message</h5><p>%@</p>",
							 commonHtmlTitle,
							 [CRecognitionViewController stringFromMocrErrorCode:errorCode],
							 errorMessage];
	[resultsView loadHTMLString:htmlMessage baseURL:nil];
}

- (void) recognizeImage:(UIImage*)image
{
	CMocrEngine* mobileOcrEngine = [CMocrEngine getSharedEngine];
	NSObject<IMocrRecognitionManager>* recognitionManager =
		[mobileOcrEngine newRecognitionManagerWithConfiguration:[self ocrConfiguration]];
	[self processRecognitionOperation:[CRecognizeTextOnImageOperation operationWithRecognitionManager:recognitionManager imageToRecognize:image callbackObject:self]];
}

- (void) recognizeBusinessCard:(UIImage*)image
{
	CMocrEngine* mobileOcrEngine = [CMocrEngine getSharedEngine];
	NSObject<IMocrRecognitionManager>* recognitionManager =
		[mobileOcrEngine newRecognitionManagerWithConfiguration:[self bcrConfiguration]];
	[self processRecognitionOperation:[CRecognizeBusinessCardOnImageOperation operationWithRecognitionManager:recognitionManager imageToRecognize:image callbackObject:self]];
}

- (void) recognizeBarcode:(UIImage*)image
{
	CMocrEngine* mobileOcrEngine = [CMocrEngine getSharedEngine];
	NSObject<IMocrRecognitionManager>* recognitionManager =
		[mobileOcrEngine newRecognitionManagerWithConfiguration:[self ocrConfiguration]];
	[self processRecognitionOperation:[CRecognizeBarcodeOnImageOperation operationWithRecognitionManager:recognitionManager imageToRecognize:image callbackObject:self]];
}

- (void) processRecognitionOperation:(CRecognitionOperation*)operation
{
	UIImage* image = operation.imageToRecognize;
	if( image != nil ) {
		imageView.image = image;
	}
	
	[self onBeforeRecognition];
		
	[self presentProgressSheet];
	[_recognitionService addOperation:operation];
}

- (void) presentProgressSheet
{
	if(progressSheet == nil) {
		progressSheet = [[UIActionSheet alloc] initWithTitle:@"Recognition in progress"
													 delegate:self	
											cancelButtonTitle:@"Cancel" 
									   destructiveButtonTitle:nil
											otherButtonTitles:nil];
		progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(50.0f, 35.0f, 220.0f, 35.0f)];
		[progressView setProgressViewStyle:UIProgressViewStyleDefault];
		[progressSheet addSubview:progressView];
	}
	
	[progressView setProgress:0.0f];
	UIWindow* topWindow = [[[UIApplication sharedApplication] delegate] window];
	if([topWindow.subviews containsObject:self.view]) {
		[progressSheet showInView:self.view];
	} else {
		[progressSheet showInView:topWindow];
	}
}

- (void) actionSheet:(UIActionSheet*)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if( buttonIndex == 0 ) {
		_needToStopRecognition = YES;
	}
}

- (void) viewDidDisappear:(BOOL)animated
{
	[resultsView loadHTMLString:@"" baseURL:nil];
}

// Set value for progress bar.
// progress object should be release.
- (void) setProgress:(NSNumber*)progress
{
	progressView.progress = [progress floatValue];
}

- (BOOL) calledWithProgress:(int)progress warningCode:(TMocrWarningCode)warningCode
{
	CGFloat stage = progress / 100.0;
	
	[self performSelectorOnMainThread:@selector(setProgress:) withObject:[[NSNumber alloc] initWithFloat:stage] waitUntilDone:NO];
	
	return !_needToStopRecognition;
}

- (void) onRotationTypeDetected:(CMocrRotationType*)rotationType
{
}

- (void) onPrebuiltWordsInfoReady:(CMocrPrebuiltLayoutInfo*)layoutInfo
{
}

- (void) onRecognizeTextOnImageSucceedWithLayout:(CMocrLayout*)layout
									rotationType:(CMocrRotationType*)rotationType
{
	[self showResults:[self htmlFromMocrLayout:layout]];
	
	// Release arguments.

	[self onAfterRecognition];
}

- (void) onRecognizeTextOnImageRegionSucceedWithLayout:(CMocrLayout*)layout
										  rotationType:(CMocrRotationType*)rotationType
{
	[self onRecognizeTextOnImageSucceedWithLayout:layout rotationType:rotationType];
}

- (void) onRecognizeBusinessCardOnImageSucceedWithBusinessCard:(CMocrBusinessCard*)businessCard
												  rotationType:(CMocrRotationType*)rotationType
{
	[self showResults:[self htmlFromMocrBusinessCard:businessCard]];

	// Release arguments.

	[self onAfterRecognition];
}

- (void) onRecognizeBarcodeOnImageSucceedWithBarcode:(CMocrBarcode*)barcode
{
	[self showResults:[self htmlFromMocrBarcode:barcode]];

	// Release argument.
	
	[self onAfterRecognition];
}

- (void) onRecognitionFailedWithErrorCode:(CMocrErrorCode*)errorCode errorMessage:(NSString*)errorMessage
{
	if( [errorCode errorCode] == MEC_FineErrTerminatedByCallback ) {
		[self.navigationController popViewControllerAnimated:YES];
	} else {
		[self showMocrError:[errorCode errorCode] message:errorMessage];
		[self onAfterRecognition];
	}
}

- (void) onBeforeRecognition
{
	_needToStopRecognition = NO;
}

- (void) onAfterRecognition
{
	[progressSheet dismissWithClickedButtonIndex:-1 animated:YES]; 
}

- (IBAction)didTapTryAgainButton:(UIButton *)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
