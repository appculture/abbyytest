// Copyright (С) ABBYY (BIT Software), 1993 - 2012. All rights reserved. 

#import <UIKit/UIKit.h>

#import <MocrEngine.h>
#import "RecognitionService.h"

@interface CRecognitionViewController : UIViewController <UINavigationBarDelegate, UIActionSheetDelegate, IRecognitionOperationCallback> 
{
	CMocrEngine* _mocrEngine;
	CMocrRecognitionConfiguration* _ocrConfiguration;
	CMocrRecognitionConfiguration* _bcrConfiguration;
	
	CRecognitionService* _recognitionService;
	
	BOOL _needToStopRecognition;
	
	NSString* _pathToData;
	
	UIActionSheet* progressSheet;
	UIProgressView* progressView;
}

@property(nonatomic, weak) IBOutlet UIImageView* imageView;
@property(nonatomic, weak) IBOutlet UIWebView* resultsView;

- (void) initializeRecognitionEngine;

- (NSString*) pathToData;

- (CMocrRecognitionConfiguration*) ocrConfiguration;
- (CMocrRecognitionConfiguration*) bcrConfiguration;

// Set value for progress bar.
// progress object should be release.
- (void) setProgress:(NSNumber*)progress;

- (void) presentProgressSheet;

// Represent CMocrLayout as html string to show in UIWebView.
- (NSString*) htmlFromMocrLayout:(CMocrLayout*)layout;
// Represent CMocrBusinessCard as html string to show in UIWebView.
- (NSString*) htmlFromMocrBusinessCard:(CMocrBusinessCard*)businessCard;
// Represent CMocrBarcode as html string to show in UIWebView.
- (NSString*) htmlFromMocrBarcode:(CMocrBarcode*)barcode;

- (void) showResults:(NSString*)stringToOutput;

- (void) showMocrError:(TMocrErrorCode)errorCode message:(NSString*)errorMessage;

- (void) recognizeImage:(UIImage*)image;

- (void) recognizeBusinessCard:(UIImage*)image;

- (void) recognizeBarcode:(UIImage*)image;

- (void) processRecognitionOperation:(CRecognitionOperation*)operation;

- (void) onBeforeRecognition;

- (void) onAfterRecognition;

+ (NSString*) stringFromMocrErrorCode:(TMocrErrorCode)errorCode;

@end
