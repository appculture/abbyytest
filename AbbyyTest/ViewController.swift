//
//  ViewController.swift
//  AbbyyTest
//
//  Created by Marko Tadic on 11/28/16.
//  Copyright © 2016 appculture. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var recognitionController = CRecognitionViewController()

    @IBAction func didTapOCRButton(_ sender: UIButton) {
        
        let imagePickerActionSheet = UIAlertController(title: "Snap/Upload Photo",
                                                       message: nil, preferredStyle: .alert)
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let cameraButton = UIAlertAction(title: "Take Photo",
                                             style: .default) { (alert) -> Void in
                                                let imagePicker = UIImagePickerController()
                                                imagePicker.delegate = self
                                                imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
                                                imagePicker.sourceType = .camera
                                                self.present(imagePicker,
                                                             animated: true,
                                                             completion: nil)
            }
            imagePickerActionSheet.addAction(cameraButton)
        }
        
        let libraryButton = UIAlertAction(title: "Choose Existing",
                                          style: .default) { (alert) -> Void in
                                            let imagePicker = UIImagePickerController()
                                            imagePicker.delegate = self
                                            imagePicker.modalPresentationStyle = UIModalPresentationStyle.currentContext
                                            imagePicker.sourceType = .photoLibrary
                                            self.present(imagePicker,
                                                         animated: true,
                                                         completion: nil)
                                            
        }
        imagePickerActionSheet.addAction(libraryButton)
        
        let cancelButton = UIAlertAction(title: "Cancel",
                                         style: .cancel) { (alert) -> Void in
        }
        imagePickerActionSheet.addAction(cancelButton)
        
        self.present(imagePickerActionSheet, animated: true,
                     completion: nil)
        
    }

}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let selectedPhoto = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        dismiss(animated: true, completion: {
            self.present(self.recognitionController, animated: true, completion: {
                self.recognitionController.recognizeBusinessCard(selectedPhoto)
            })
        })
    }
    
}
